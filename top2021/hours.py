#!/usr/bin/python3

import json

with open('songs-unpruned.json') as f:
    songs = json.load(f)

output = []
lastTime = ''
for song in reversed(songs['positions']):
    if song['broadcastTime'] != lastTime:
        lastTime = song['broadcastTime']
        print(song['position'])

