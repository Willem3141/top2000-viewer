[
    {"day": 25, "hour":  0, "start_id": 2000},
    {"day": 25, "hour":  1, "start_id": 1989},
    {"day": 25, "hour":  2, "start_id": 1975},
    {"day": 25, "hour":  3, "start_id": 1961},
    {"day": 25, "hour":  4, "start_id": 1948},
    {"day": 25, "hour":  5, "start_id": 1934},
    {"day": 25, "hour":  6, "start_id": 1920},
    {"day": 25, "hour":  7, "start_id": 1907},
    {"day": 25, "hour":  8, "start_id": 1895},
    {"day": 25, "hour":  9, "start_id": 1883},
    {"day": 25, "hour": 10, "start_id": 1871},
    {"day": 25, "hour": 11, "start_id": 1859},
    {"day": 25, "hour": 12, "start_id": 1846},
    {"day": 25, "hour": 13, "start_id": 1834},
    {"day": 25, "hour": 14, "start_id": 1823},
    {"day": 25, "hour": 15, "start_id": 1812},
    {"day": 25, "hour": 16, "start_id": 1799},
    {"day": 25, "hour": 17, "start_id": 1791},
    {"day": 25, "hour": 18, "start_id": 1776},
    {"day": 25, "hour": 19, "start_id": 1766},
    {"day": 25, "hour": 20, "start_id": 1754},
    {"day": 25, "hour": 21, "start_id": 1739},
    {"day": 25, "hour": 22, "start_id": 1725},
    {"day": 25, "hour": 23, "start_id": 1712},
    {"day": 26, "hour":  0, "start_id": 1698},
    {"day": 26, "hour":  1, "start_id": 1684},
    {"day": 26, "hour":  2, "start_id": 1672},
    {"day": 26, "hour":  3, "start_id": 1658},
    {"day": 26, "hour":  4, "start_id": 1645},
    {"day": 26, "hour":  5, "start_id": 1633},
    {"day": 26, "hour":  6, "start_id": 1618},
    {"day": 26, "hour":  7, "start_id": 1605},
    {"day": 26, "hour":  8, "start_id": 1595},
    {"day": 26, "hour":  9, "start_id": 1583},
    {"day": 26, "hour": 10, "start_id": 1571},
    {"day": 26, "hour": 11, "start_id": 1559},
    {"day": 26, "hour": 12, "start_id": 1547},
    {"day": 26, "hour": 13, "start_id": 1534},
    {"day": 26, "hour": 14, "start_id": 1521},
    {"day": 26, "hour": 15, "start_id": 1508},
    {"day": 26, "hour": 16, "start_id": 1496},
    {"day": 26, "hour": 17, "start_id": 1484},
    {"day": 26, "hour": 18, "start_id": 1472},
    {"day": 26, "hour": 19, "start_id": 1460},
    {"day": 26, "hour": 20, "start_id": 1447},
    {"day": 26, "hour": 21, "start_id": 1434},
    {"day": 26, "hour": 22, "start_id": 1420},
    {"day": 26, "hour": 23, "start_id": 1406},
    {"day": 27, "hour":  0, "start_id": 1392},
    {"day": 27, "hour":  1, "start_id": 1379},
    {"day": 27, "hour":  2, "start_id": 1365},
    {"day": 27, "hour":  3, "start_id": 1351},
    {"day": 27, "hour":  4, "start_id": 1339},
    {"day": 27, "hour":  5, "start_id": 1325},
    {"day": 27, "hour":  6, "start_id": 1312},
    {"day": 27, "hour":  7, "start_id": 1302},
    {"day": 27, "hour":  8, "start_id": 1290},
    {"day": 27, "hour":  9, "start_id": 1279},
    {"day": 27, "hour": 10, "start_id": 1268},
    {"day": 27, "hour": 11, "start_id": 1256},
    {"day": 27, "hour": 12, "start_id": 1243},
    {"day": 27, "hour": 13, "start_id": 1234},
    {"day": 27, "hour": 14, "start_id": 1224},
    {"day": 27, "hour": 15, "start_id": 1212},
    {"day": 27, "hour": 16, "start_id": 1200},
    {"day": 27, "hour": 17, "start_id": 1186},
    {"day": 27, "hour": 18, "start_id": 1173},
    {"day": 27, "hour": 19, "start_id": 1161},
    {"day": 27, "hour": 20, "start_id": 1148},
    {"day": 27, "hour": 21, "start_id": 1134},
    {"day": 27, "hour": 22, "start_id": 1121},
    {"day": 27, "hour": 23, "start_id": 1109},
    {"day": 28, "hour":  0, "start_id": 1097},
    {"day": 28, "hour":  1, "start_id": 1083},
    {"day": 28, "hour":  2, "start_id": 1070},
    {"day": 28, "hour":  3, "start_id": 1055},
    {"day": 28, "hour":  4, "start_id": 1041},
    {"day": 28, "hour":  5, "start_id": 1028},
    {"day": 28, "hour":  6, "start_id": 1016},
    {"day": 28, "hour":  7, "start_id": 1005},
    {"day": 28, "hour":  8, "start_id": 993 },
    {"day": 28, "hour":  9, "start_id": 980 },
    {"day": 28, "hour": 10, "start_id": 967 },
    {"day": 28, "hour": 11, "start_id": 956 },
    {"day": 28, "hour": 12, "start_id": 949 },
    {"day": 28, "hour": 13, "start_id": 938 },
    {"day": 28, "hour": 14, "start_id": 927 },
    {"day": 28, "hour": 15, "start_id": 916 },
    {"day": 28, "hour": 16, "start_id": 904 },
    {"day": 28, "hour": 17, "start_id": 892 },
    {"day": 28, "hour": 18, "start_id": 881 },
    {"day": 28, "hour": 19, "start_id": 868 },
    {"day": 28, "hour": 20, "start_id": 856 },
    {"day": 28, "hour": 21, "start_id": 843 },
    {"day": 28, "hour": 22, "start_id": 831 },
    {"day": 28, "hour": 23, "start_id": 818 },
    {"day": 29, "hour":  0, "start_id": 808 },
    {"day": 29, "hour":  1, "start_id": 795 },
    {"day": 29, "hour":  2, "start_id": 782 },
    {"day": 29, "hour":  3, "start_id": 771 },
    {"day": 29, "hour":  4, "start_id": 759 },
    {"day": 29, "hour":  5, "start_id": 747 },
    {"day": 29, "hour":  6, "start_id": 734 },
    {"day": 29, "hour":  7, "start_id": 721 },
    {"day": 29, "hour":  8, "start_id": 708 },
    {"day": 29, "hour":  9, "start_id": 697 },
    {"day": 29, "hour": 10, "start_id": 685 },
    {"day": 29, "hour": 11, "start_id": 672 },
    {"day": 29, "hour": 12, "start_id": 661 },
    {"day": 29, "hour": 13, "start_id": 651 },
    {"day": 29, "hour": 14, "start_id": 639 },
    {"day": 29, "hour": 15, "start_id": 628 },
    {"day": 29, "hour": 16, "start_id": 616 },
    {"day": 29, "hour": 17, "start_id": 604 },
    {"day": 29, "hour": 18, "start_id": 594 },
    {"day": 29, "hour": 19, "start_id": 583 },
    {"day": 29, "hour": 20, "start_id": 571 },
    {"day": 29, "hour": 21, "start_id": 561 },
    {"day": 29, "hour": 22, "start_id": 550 },
    {"day": 29, "hour": 23, "start_id": 537 },
    {"day": 30, "hour":  0, "start_id": 524 },
    {"day": 30, "hour":  1, "start_id": 511 },
    {"day": 30, "hour":  2, "start_id": 498 },
    {"day": 30, "hour":  3, "start_id": 485 },
    {"day": 30, "hour":  4, "start_id": 472 },
    {"day": 30, "hour":  5, "start_id": 458 },
    {"day": 30, "hour":  6, "start_id": 446 },
    {"day": 30, "hour":  7, "start_id": 436 },
    {"day": 30, "hour":  8, "start_id": 422 },
    {"day": 30, "hour":  9, "start_id": 413 },
    {"day": 30, "hour": 10, "start_id": 401 },
    {"day": 30, "hour": 11, "start_id": 391 },
    {"day": 30, "hour": 12, "start_id": 381 },
    {"day": 30, "hour": 13, "start_id": 372 },
    {"day": 30, "hour": 14, "start_id": 360 },
    {"day": 30, "hour": 15, "start_id": 350 },
    {"day": 30, "hour": 16, "start_id": 338 },
    {"day": 30, "hour": 17, "start_id": 327 },
    {"day": 30, "hour": 18, "start_id": 314 },
    {"day": 30, "hour": 19, "start_id": 303 },
    {"day": 30, "hour": 20, "start_id": 292 },
    {"day": 30, "hour": 21, "start_id": 280 },
    {"day": 30, "hour": 22, "start_id": 274 },
    {"day": 30, "hour": 23, "start_id": 262 },
    {"day": 31, "hour":  0, "start_id": 251 },
    {"day": 31, "hour":  1, "start_id": 240 },
    {"day": 31, "hour":  2, "start_id": 228 },
    {"day": 31, "hour":  3, "start_id": 215 },
    {"day": 31, "hour":  4, "start_id": 202 },
    {"day": 31, "hour":  5, "start_id": 189 },
    {"day": 31, "hour":  6, "start_id": 178 },
    {"day": 31, "hour":  7, "start_id": 166 },
    {"day": 31, "hour":  8, "start_id": 155 },
    {"day": 31, "hour":  9, "start_id": 145 },
    {"day": 31, "hour": 10, "start_id": 134 },
    {"day": 31, "hour": 11, "start_id": 123 },
    {"day": 31, "hour": 12, "start_id": 112 },
    {"day": 31, "hour": 13, "start_id": 103 },
    {"day": 31, "hour": 14, "start_id": 92  },
    {"day": 31, "hour": 15, "start_id": 82  },
    {"day": 31, "hour": 16, "start_id": 71  },
    {"day": 31, "hour": 17, "start_id": 63  },
    {"day": 31, "hour": 18, "start_id": 52  },
    {"day": 31, "hour": 19, "start_id": 43  },
    {"day": 31, "hour": 20, "start_id": 35  },
    {"day": 31, "hour": 21, "start_id": 26  },
    {"day": 31, "hour": 22, "start_id": 17  },
    {"day": 31, "hour": 23, "start_id": 8   },
    {"day":  1, "hour":  0, "start_id": 0   }
]
